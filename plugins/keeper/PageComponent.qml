/*
 * This file is part of lomiri-system-settings-keeper
 *
 * Copyright (C) 2023 UBports Foundation
 *
 * Author: Alfred Neumayer <dev.beidl@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import SystemSettings 1.0
import SystemSettings.ListItems 1.0 as SettingsListItems
import Lomiri.Components.ListItems 1.3 as LomiriListItems
import Lomiri.Components 1.3
import Ubuntu.Keeper 0.1

ItemPage {
    id: root
    title: i18n.tr("Backup & Restore")
    flickable: keeperFlickable

    Keeper {
        id: keeper
    }

    function startBackup() {
        for (let i = 0; i < keeper.backupUuids.length; i++) {
            keeper.enableBackup(keeper.backupUuids[i], true)
            keeper.enableRestore(keeper.backupUuids[i], false)
        }
        keeper.startBackup("default")
    }

    function startRestore() {
        for (let i = 0; i < keeper.backupUuids.length; i++) {
            keeper.enableBackup(keeper.backupUuids[i], false)
            keeper.enableRestore(keeper.backupUuids[i], true)
        }
        keeper.startRestore("default")
    }

    Flickable {
        id: keeperFlickable
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        boundsBehavior: (contentHeight > root.height) ?
                            Flickable.DragAndOvershootBounds :
                            Flickable.StopAtBounds
        /* Workaround https://bugreports.qt-project.org/browse/QTBUG-31905 */
        flickableDirection: Flickable.VerticalFlick

        Column {
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(2)

            ProgressBar {
                id: progressBar
                anchors {
                    left: parent.left
                    right: parent.right
                }
                minimumValue: 0.0
                maximumValue: 1.0
                value: keeper.progress
                visible: keeper.backupBusy
            }

            RowLayout {
                id: status
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                spacing: units.gu(2)

                Behavior on opacity {
                    LomiriNumberAnimation {}
                }
                Behavior on height {
                    LomiriNumberAnimation {}
                }

                opacity: visible ? 1 : 0
                height: visible ? units.gu(6) : 0
                visible: keeper.backupBusy

                Label {
                    objectName: "backupLabel"
                    // // TRANSLATORS: %1 is the current status
                    text: i18n.tr("Backing up: %1...").arg(keeper.status)
                    Layout.fillWidth: true
                }

                Button {
                    objectName: "cancelButton"
                    text: i18n.tr("Cancel");
                    onClicked: keeper.cancel();
                    color: theme.palette.normal.positive
                }

                ActivityIndicator {
                    running: keeper.backupBusy
                }
            }

            LomiriListItems.Caption {
                text: i18n.tr("Backups store media and application settings on cloud-connected storage.")
                width: parent.width
            }

            LomiriListItems.ItemSelector {
                text: i18n.tr("Account")
                model: keeper.getStorageAccounts()
            }

            SettingsListItems.ThinDivider {
                visible: true
                width: parent.width
            }

            Row {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: units.gu(2)

                Button {
                    text: i18n.tr("Create backup")
                    width: (parent.width / 2) - units.gu(2)
                    onClicked: startBackup()
                }

                Button {
                    text: i18n.tr("Restore backup")
                    width: (parent.width / 2) - units.gu(2)
                    onClicked: startRestore()
                }
            }
        }
    }
}
